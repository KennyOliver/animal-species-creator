# animal-species-creator :unicorn:

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/animal-species-creator/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/animal-species-creator?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/animal-species-creator?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/animal-species-creator?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/animal-species-creator)](https://replit.com/@KennyOliver/animal-species-creator)

**Create imaginary animal species using OOP!**

## VividHues :rainbow: :package:
**animal-species-creator** uses **VividHues** - my own Python Package!

[![VividHues](https://img.shields.io/badge/Get%20VividHues-252525?style=for-the-badge&logo=python&logoColor=white&link=https://github.com/KennyOliver/VividHues)](https://github.com/KennyOliver/VividHues)

---
Kenny Oliver ©2021
